//
//  DescriptionView.swift
//  GoJekUser
//
//  Created by AppleMac on 10/08/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class DescriptionView: UIView {

    @IBOutlet weak var Labeltitle: UILabel!
    @IBOutlet weak var LabelDescription: UILabel!
    @IBOutlet weak var butOkay: UIButton!
    @IBOutlet weak var backGroundView: UIView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        initialLoad()
    }
    
  
    
    func initialLoad() {
       
        self.Labeltitle.text = "Oihelp"
        self.Labeltitle.textColor = .foodieColor
       
        butOkay.addTarget(self, action: #selector(Okayaction), for: .touchUpInside)
      
       
        self.backGroundView.addShadow(radius: 7.0, color: .gray)
    }
    
    @objc func Okayaction() {
     
        self.removeFromSuperview()
        
    }
    
    
   
}
