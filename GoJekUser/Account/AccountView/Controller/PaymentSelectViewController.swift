//
//  PaymentSelectViewController.swift
//  GoJekProvider
//
//  Created by apple on 16/05/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import CFSDK

class PaymentSelectViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var paymentTypeArr: [PaymentDetails] = Array()
    var cardsList = CardEntityResponse()
    var cardFooterView: CardView?
    var isFromAddAmountWallet: Bool = false
    var isChangePayment: Bool = false
    var walletAmount: String = ""
    var onClickPayment: ((PaymentType, CardResponseData?)-> Void)? //change payment mode from request
    var selectCardId:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = isFromAddAmountWallet == true || isChangePayment == true ? AccountConstant.choosePayment.localized : AccountConstant.payment.localized
        // Do any additional setup after loading the view.
        
        self.accountPresenter?.getCard()
        //Get card status
        self.accountPresenter?.postAppsettings(param: [LoginConstant.salt_key: APPConstant.salt_key])
        self.setNavigationTitle()
        self.setLeftBarButtonWith(color: .blackColor)
        self.view.backgroundColor = .veryLightGray
        self.tableView.register(nibName: AccountConstant.PaymentTypeTableViewCell)
        self.tableView.register(nibName: AccountConstant.CardView)
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.hideTabBar()
        self.hideTabBar()
        setDarkMode()
    }
    
    private func setDarkMode(){
        self.view.backgroundColor = .backgroundColor
        self.tableView.backgroundColor = .backgroundColor
    }
}

// MARK: - UITableViewDataSource

extension PaymentSelectViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.paymentTypeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PaymentTypeTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: AccountConstant.PaymentTypeTableViewCell, for: indexPath) as! PaymentTypeTableViewCell
        cell.selectionStyle = .none
        
        let paymentDic = self.paymentTypeArr[indexPath.row]
        cell.setPaymentValue(payment: paymentDic)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if paymentTypeArr.contains(where: { ($0.name)?.uppercased()  == Constant.cash.uppercased() }) {
            return self.cardFooterView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
       
        if paymentTypeArr.contains(where: { ($0.name)?.uppercased()  == Constant.cash.uppercased() }) {
            let header = view as? UITableViewHeaderFooterView
            header?.backgroundColor = .black
            header?.textLabel?.font = UIFont.setCustomFont(name: .medium, size: .x18)
            header?.textLabel?.textColor = .black
            header?.textLabel?.text = AccountConstant.availablePayment.localized
        }
    }
}

// MARK: - UITableViewDelegate

extension PaymentSelectViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
        return self.cardFooterView?.frame.height ?? 0
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if paymentTypeArr.contains(where: { ($0.name)?.uppercased()  == Constant.cash.uppercased() }) {
            return 40
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isChangePayment {
            if self.paymentTypeArr[indexPath.row].name?.uppercased() == "CASH"{
            self.onClickPayment?(.CASH, nil)
            }else if self.paymentTypeArr[indexPath.row].name?.uppercased() == "CASHFREE"{
                self.onClickPayment?(.CASHFREESDK, nil)
            }
            self.navigationController?.popViewController(animated: true)
        }
        
        addAmountToWallet()
        
    }
    
    
    
}

extension PaymentSelectViewController: AccountPresenterToAccountViewProtocol {
    
    func getCardResponse(getCardResponse: CardEntityResponse) {
        cardsList = getCardResponse
        cardFooterView?.isDeleteCancelShow = true
        cardFooterView?.isFromAnotherPage = isFromAddAmountWallet
        cardFooterView?.cardsList = getCardResponse
        cardFooterView?.selectedCard_token = selectCardId
        for i in 0..<(cardsList.responseData?.count ?? 0) {
            if cardsList.responseData?[i].card_id == selectCardId {
                cardFooterView?.selectedCardIndex = i
                cardFooterView?.selectedCardId = cardsList.responseData?[i].id ?? 0
            }
        }
        cardFooterView?.paymentCardCollectionView.reloadInMainThread()
    }
    
    func deleteCardSuccess(deleteCardResponse: CardEntityResponse) {
        
        self.accountPresenter?.getCard()
    }
    
    func addMoneyToWalletSuccess(walletSuccessResponse: AddAmountEntity) {
        guard let cfToken = walletSuccessResponse.responseData?.cftoken else {
            ToastManager.show(title: walletSuccessResponse.responseData?.message ?? AccountConstant.AddAmountSuccess, state: .success)
            self.navigationController?.popViewController(animated: true)
            return
        }
        print("CFToke",cfToken)
        self.initiateCF(cftoken: cfToken,order_id : walletSuccessResponse.responseData?.order_id ?? "")
    }
    
    func addNewCardButtonClick() {
        if guestLogin() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: OrderConstant.AddCardViewController) as! AddCardViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func CashFreeResponse(baseEntity: CashFreeEntity) {
        ToastManager.show(title: baseEntity.message ?? AccountConstant.AddAmountSuccess, state: .success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func postAppsettingsResponse(baseEntity: BaseEntity) {
        
        self.accountPresenter?.getCard()
        self.paymentTypeArr.removeAll()
        
        AppConfigurationManager.shared.baseConfigModel = baseEntity
        AppConfigurationManager.shared.setBasicConfig(data: baseEntity)
        
        let paymetArray = baseEntity.responseData?.appsetting?.payments ?? []
        var tempPaymentArray: [PaymentDetails] = Array()
        for paymentDic in paymetArray {
            if paymentDic.status == "1" {
                paymentTypeArr.append(paymentDic)
                tempPaymentArray.append(paymentDic)
            }
        }
        
        if isFromAddAmountWallet == true, let index = paymentTypeArr.firstIndex(where: { $0.name?.uppercased() ==  Constant.cash.uppercased()}) {
            paymentTypeArr.remove(at: index)
        }
        
        if let _ = paymentTypeArr.firstIndex(where: { $0.name?.uppercased() ==  Constant.card.uppercased()}) {
            if let index = paymentTypeArr.firstIndex(where: { $0.name?.uppercased() ==  Constant.card.uppercased()}) {
                paymentTypeArr.remove(at: index)
            }
            if self.cardFooterView == nil, let cardFooterView = Bundle.main.loadNibNamed(AccountConstant.CardView, owner: self, options: [:])?.first as? CardView {
                cardFooterView.frame = CGRect(x: 16, y: self.paymentTypeArr.count>0 ? 0 : 100, width: self.view.frame.width-32, height: cardFooterView.frame.height)
                cardFooterView.backgroundColor = .backgroundColor
                cardFooterView.delegate = self
                self.cardFooterView = cardFooterView
                self.view.addSubview(cardFooterView)
            }
        }
        
        if tempPaymentArray.count == 0 {
            self.tableView.setBackgroundImageAndTitle(imageName: Constant.ic_empty_card, title: AccountConstant.noPaymentType,tintColor: .black)
        }
        else {
            self.tableView.backgroundView = nil
        }
        self.tableView.reloadInMainThread()
    }
}

extension PaymentSelectViewController: CardViewDelegate {
    
    func deleteButtonClick() {
        AppAlert.shared.simpleAlert(view: self, title: AccountConstant.deleteMsg, message: String.empty, buttonOneTitle: Constant.SYes, buttonTwoTitle: Constant.SNo)
        AppAlert.shared.onTapAction = { [weak self] tag in
            guard let self = self else {
                return
            }
            if tag == 0 {
                self.accountPresenter?.deleteCard(cardID: self.cardFooterView?.selectedCardId ?? 0 )
            }
        }
    }
    
    func addAmountToWallet() {
        if isFromAddAmountWallet {
            let param: Parameters = [AccountConstant.amount: walletAmount,
                                    // AccountConstant.card_id: cardFooterView?.selectedCard_token ?? "",
                                     AccountConstant.user_type: userType.user.rawValue,
                                     AccountConstant.payment_Mode: "CASHFREESDK"]
            
            self.accountPresenter?.addMoneyToWallet(param: param)
        }
        else if isChangePayment {
            if cardsList.responseData?.count ?? 0 > 0 {
                let indexVal: Int = cardFooterView?.selectedCardIndex ?? 0
                let cardDetail = cardsList.responseData?[indexVal]
                self.onClickPayment?(.CASHFREESDK,cardDetail)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}


extension PaymentSelectViewController: ResultDelegate {
    
    func initiateCF(cftoken : String,order_id : String){
        CFPaymentService().doWebCheckoutPayment(params: getPaymentParams(cftoken: cftoken,order_id : order_id),env: "env",callback: self)
    }
    
    func getPaymentParams(cftoken : String,order_id : String) -> Dictionary<String, Any> {
        let num = Int.random(in: 100000..<999999)
        return [
            "appId": APPConstant.CashFreeAppID,
            "orderId": "\(order_id)",
            "tokenData" : "\(cftoken)",
            "orderAmount": "\(walletAmount)",
            "customerName": AppManager.shared.getUserDetails()?.first_name ?? "",
            "orderNote": "Order Note",
            "orderCurrency": "INR",
            "customerPhone": AppManager.shared.getUserDetails()?.mobile ?? "",
            "customerEmail": AppManager.shared.getUserDetails()?.email ?? "",
            "notifyUrl": "https://test.gocashfree.com/notify"
        ]
    }
    
    func onPaymentCompletion(msg: String) {
        print("Result Delegate : onPaymentCompletion")
        print(msg)
        let response = try? JSONDecoder().decode(CFReponse.self, from: msg.data(using: .utf8)!)
        print("response",response)
        // Handle the result here
        
        
        var param : Parameters?
        do {
            param = try JSONSerialization.jsonObject(with: msg.data(using: .utf8)!, options: []) as? [String: Any]
                } catch {
                    print(error.localizedDescription)
                }
        
        self.accountPresenter?.VerifyPayment(param: param!)
    }
}


struct CFReponse: Decodable {
    var orderId: String
    var referenceId: String
    var orderAmount: String
    var txStatus: String
    var txMsg: String
    var txTime: String
    var paymentMode: String
}
